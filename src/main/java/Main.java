import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileManager;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


class PostgreSQLJDBC {
    public static void main(String args[]) {
        Connection c = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            Class.forName ( "org.postgresql.Driver" );
            c = DriverManager
                    .getConnection ( "jdbc:postgresql://localhost:5432/Student",
                            "postgres", "liMA" );
            c.setAutoCommit ( false );
            System.out.println ( "Opened database successfully" );

            stmt = c.createStatement ();
            rs = stmt.executeQuery ( "SELECT * FROM Student;" );
            while (rs.next ()) {
                int id = rs.getInt ( "Student_id" );
                String fname = rs.getString ( "Fname" );
                String lname = rs.getString ( "Lname" );
                int age = rs.getInt ( "age" );
                String address = rs.getString ( "group_name" );

                System.out.println ( "ID = " + id );
                System.out.println ( "FIRST NAME = " + fname );
                System.out.println ("Last NAME = " + lname);
                System.out.println ( "AGE = " + age );
                System.out.println ( "GROUP = " + address );

                System.out.println ();
            }

        } catch (Exception e) {
            System.err.println ( e.getClass ().getName () + ": " + e.getMessage () );
            System.exit ( 0 );
        } finally {
            try {
                rs.close ();
                stmt.close ();
                c.close ();
            } catch (Exception e) {
                System.err.println ( e.getClass ().getName () + ": " + e.getMessage () );
                System.exit ( 0 );
            }


        }
        System.out.println ( "Operation done successfully" );
    }
}